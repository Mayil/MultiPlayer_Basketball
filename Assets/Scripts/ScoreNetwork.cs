﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Networking;

public class ScoreNetwork : NetworkBehaviour {

    public static ScoreNetwork instance;
    [SyncVar]
    public int score = 0;
    [SyncVar]
    public string uname = "player";
    [SerializeField]
    GameObject effect;


    AudioManager audioMan;

    public Text points, name;

    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        //audioMan = AudioManager.instance;
        setScore();
        if (isLocalPlayer)
        {
            effect = transform.Find("+2").gameObject;
            effect.SetActive(false);
        }

    }

    // Update is called once per frame
    void Update()
    {
       // if (isLocalPlayer)
        {
            // Debug.Log(score);
            points.text = "Score:" + score.ToString();
            name.text = uname;
        }
        // else
        {
            // Debug.Log(score);
            // points.text = "Score:" + score.ToString();
        }
    }

    public void setScore()
    {
        Debug.Log("ScoreSet");

        //if(isLocalPlayer)
        points.text = "Score:" + score.ToString();
        //else
        // points.text = "Score:" + score.ToString();

        if (isLocalPlayer)
            effect.SetActive(false);
    }


  

    public void scoreChange()
    {
        if (!isServer)
            Debug.Log("NotActiveOnServer");
        Debug.Log("Entered");
        if(isLocalPlayer)
        CmdscoreChange();
        //RpcscoreChange();
    }
    [Command]
    void CmdscoreChange()
    {
        Debug.Log("Entering");
        //if (!isServer)
        {
            // Debug.Log("Not active on Server");
            //return;
        }
        
        score += 2;

    }

    //[ClientRpc]
    void RpcscoreChange()
    {
        //Debug.Log("Entering");
        //if (!isServer)
        {
            // Debug.Log("Not active on Server");
            //return;
        }
        score += 2;
        if (isClient)
            Debug.Log(score);
        else
            Debug.Log(score);
    }

    void OnPlayerConnected()
    {
        score = 0;
        setScore();
        OnChangeScore(score);
    }
    void OnChangeScore(int scre)
    {
        Debug.Log("Client Score Changed");
        score = scre;
        setScore();
    }

    void OnGUI()
    {
        if (isLocalPlayer)
        {
            //Debug.Log("GUIcreated");
            uname = GUI.TextField(new Rect(25, Screen.height - 40, 100, 30), uname);
            if (GUI.Button(new Rect(130, Screen.height - 40, 80, 30), "Change"))
            {
                CmdChangeName(uname);

            }
        }
    }



    [Command]
    public void CmdChangeName(string newName)
    {
        uname = newName;
    }
}
