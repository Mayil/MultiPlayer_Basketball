﻿using UnityEngine;
using UnityEngine.EventSystems;

public class EventTriggerExample : EventTrigger
{
    BallController ball;
   
    void Start()
    {
        ball = new BallController();
        ball = BallController.instance;
    }
    public override void OnPointerUp(PointerEventData data)
    {
        Debug.Log("OnPointerUp called.");
        ball.OnTouchUp();
    }

    public override void OnPointerDown(PointerEventData data)
    {
        Debug.Log("OnPointerDown called.");
        ball.OnTouchDown();
    }
}
