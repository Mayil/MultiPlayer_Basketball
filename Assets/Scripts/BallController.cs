﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System;

[RequireComponent(typeof(Rigidbody))]
public class BallController : NetworkBehaviour {

   public static BallController instance;

    //Vector2 touchOrigin = -Vector2.one;
    //float horizontal = 0;
    //float vertical = 0,x,y;
    //public GameObject effect;
    //public Text Score;
    //public GameObject sc;


    [SerializeField]
    GameObject pauseObjects;

    public Rigidbody ball;
    public float score;    
    float InitialTime, FinalTime;
    public Vector2 IntTouch, FinalTouch;

    Vector3 BallPosition;
    private float Xaxis, Yaxis, Zaxis;
    Vector3 RequireForce;
    private int horizontal;
    private int vertical;
    private int x;
    private int y;
    private Vector2 touchOrigin,touchEnd;

    //public GameManager GameMan;
    public AudioManager audioMan;
    Score scr;

        void Awake()
    {
        instance = this;
        
    }
    void Start () {
        scr = new Score();
        scr = Score.instance;
        ball = GetComponent<Rigidbody>();
        BallPosition = GetComponent<Transform>().transform.position;
        ball.useGravity=false;
       // GameMan = GameManager.instance;
        audioMan = AudioManager.instance;
        //effect.SetActive(false); 
        LeaveRoom.IsOn = false;          
    }
	
    void Update()
    {
        //Score.GetComponent<Text>().text = "Score:" + score.ToString();
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePauseMenu();
        }
    }

    private void TogglePauseMenu()
    {
        pauseObjects.SetActive(!pauseObjects.activeSelf);
        LeaveRoom.IsOn = pauseObjects.activeSelf;
    }

    public void OnTouchDown()
    {
        Debug.Log("Touched");
        //InitialTime = Time.time;
        IntTouch = Input.mousePosition;
    }

    public void OnTouchUp()
    {
        //FinalTime = Time.time;
        Debug.Log("Touched");
        FinalTouch = Input.mousePosition;
        BallThrow();
        
    }


    public void BallThrow()
    {
        Debug.Log("Moving");
        audioMan.PlaySound("Throw");
        Xaxis = FinalTouch.x - IntTouch.x;
        Yaxis = FinalTouch.y - IntTouch.y;
       // Zaxis = FinalTime - InitialTime;
        ball.useGravity = true;
        RequireForce = new Vector3(Xaxis*0.4f, Yaxis, Yaxis*0.5f);
        ball.velocity = RequireForce*0.01f;

    }

	
	/*void FixedUpdate () {
        
		if(Input.touchCount>0)
        {
            Debug.Log("Detected");
            Touch myTouch = Input.touches[0];
            if(myTouch.phase==TouchPhase.Began)
            {
                touchOrigin = myTouch.position;
            }
            else if(myTouch.phase==TouchPhase.Ended && touchOrigin.x>0)
            {
                Vector2 touchEnd = myTouch.position;
                x = (int)(touchEnd.x - touchOrigin.x)%3;
                y = (int)(touchEnd.y - touchOrigin.y)%2;
                touchOrigin.x = -1;
                touchOrigin.y = -1;
                horizontal = x;
                vertical = y;
            }
            if(x>0 || y>0)
             {
                horizontal = x > 0 ? 1 : -1;
                vertical = y > 0 ? 1 : -1;
             }
            
              ball.AddForce(new Vector3(0, vertical + 4f, horizontal+2.0f) * 0.1f, ForceMode.Impulse);
        }

       
    }*/

    public void Reload()
    {
        SceneManager.LoadScene("Game");
    }

    
     void OnTriggerEnter(Collider other)
    {
        /*if (other.tag == "Score")
        {
            scr.CmdscoreChange();
            audioMan.PlaySound("Score");
            //effect.SetActive(true);
            Debug.Log("Trigerred");
            
        }*/
        if(other.tag=="Ground")
        {
            audioMan.PlaySound("Ground");
            Debug.Log("Ground");
            gameObject.transform.position = BallPosition;
            ball.useGravity = false;
            ball.velocity = new Vector3(0, 0, 0);
            //effect.SetActive(false);
        }
            
    }

    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag=="Ring")
        {
            audioMan.PlaySound("Ring");
        }
        if(other.gameObject.tag=="Board")
        {
            audioMan.PlaySound("Board");
        }
    }

}
