﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using UnityEngine.UI;

public class PlayerNetwork : NetworkBehaviour {
    [SerializeField]Behaviour[] componentsToDisable;
    public Transform centrePoint;
    BallController ball;

    void Start () {
        
        if (isLocalPlayer)
        {
            ball = new BallController();
            ball = BallController.instance;
            GetComponentInChildren<BallController>().enabled = true;
        }
        if (!isLocalPlayer)
        {
            transform.Find("Canvas").gameObject.SetActive(false);
            GetComponentInChildren<Rigidbody>().isKinematic = true;
            for (int i = 0; i < componentsToDisable.Length; i++)
            {
                componentsToDisable[i].enabled = false;
            }
            
        }
        else
        {
            for (int i = 0; i < componentsToDisable.Length; i++)
            {
                componentsToDisable[i].enabled = true;
            }
        }

    }
    
	
  
	void Update () {
		
	}
}
