﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.UI;

public class RoomListButton : MonoBehaviour {

    public delegate void JoinRoomDelegate(MatchInfoSnapshot _match);
    public JoinRoomDelegate joinCallback;
    [SerializeField]
    Text Roomname;

    MatchInfoSnapshot match;
	public void Setup(MatchInfoSnapshot _match,JoinRoomDelegate _callback)
    {
      
        match = _match;
        joinCallback = _callback;
        Roomname.text = match.name+"("+match.currentSize+"/"+match.maxSize+")";

    }

    public void JoinGame()
    {
        joinCallback.Invoke(match);
    }
	
}
