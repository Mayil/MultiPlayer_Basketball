﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour {

    public static GameManager instance;
    [SerializeField]
    GameObject[] Exitobjs;
   // Vector2 IntTouch, FinalTouch;

    //BallController ball;

    public Animator exitAnim;
   
    public AudioManager audioMan;
	void Start () {
        instance = this;
       // ball = BallController.instance;
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToLandscapeRight = false;
        audioMan = AudioManager.instance;
        audioMan.PlaySound("BGM");
        Exitobjs = GameObject.FindGameObjectsWithTag("ExitObj");
        hideExit();
      
    }

    void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            
            showExit();
        }

    }

    private void showExit()
    {
        foreach(GameObject g in Exitobjs)
        {
            g.SetActive(true);
        }
    }

    private void hideExit()
    {
        foreach (GameObject g in Exitobjs)
        {
            g.SetActive(false);
        }
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Cancel()
    {        
        StartCoroutine("AnimationPlay");  
    }

    IEnumerator AnimationPlay()
    {
        exitAnim.SetTrigger("Cancel");
        yield return new WaitForSeconds(1f);
        hideExit();
       
    }
}
