﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Score : NetworkBehaviour {
    public static Score instance;
    [SerializeField]
    ScoreNetwork sn;
    //[SerializeField]
    [SyncVar]
    public int score = 0;
    [SyncVar]
    public string uname = "player";
    [SerializeField]
    GameObject effect;


    AudioManager audioMan;

   public Text points,name;

    void Awake()
    {
        instance = this;
    }
    void Start() {
        audioMan = AudioManager.instance;
        setScore();
        if (isLocalPlayer)
        {
            effect = transform.Find("+2").gameObject;
            effect.SetActive(false);
        }
       
    }

    // Update is called once per frame
    void Update() {
        //if (isLocalPlayer)
        {            
               // Debug.Log(score);
           // points.text = "Score:" + score.ToString();
           // name.text = uname;
        }
       // else
        {
           // Debug.Log(score);
           // points.text = "Score:" + score.ToString();
        }
    }

    public void setScore()
    {
        Debug.Log("ScoreSet");

        //if(isLocalPlayer)
            points.text = "Score:" + score.ToString();
        //else
           // points.text = "Score:" + score.ToString();

        if (isLocalPlayer)
            effect.SetActive(false);
    }
    
    
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Score")
        { 
                
            audioMan.PlaySound("Score");
            
                sn.scoreChange();
            //effect.SetActive(true);
            Debug.Log("Trigerred");
        }
       
    }

   /* public void scoreChange()
    {
        Debug.Log("Entered");
        CmdscoreChange();
        //RpcscoreChange();
    }*/
    [Command]
    void CmdscoreChange()
    {
        Debug.Log("Entering");
        //if (!isServer)
        {
           // Debug.Log("Not active on Server");
            //return;
        }
        score += 2;
        
    }

    //[ClientRpc]
    void RpcscoreChange()
    {
        //Debug.Log("Entering");
        //if (!isServer)
        {
            // Debug.Log("Not active on Server");
            //return;
        }
        score += 2;
        if (isClient)
            Debug.Log(score);
        else
            Debug.Log(score);
    }

    void OnPlayerConnected()
    {
        score = 0;
        setScore();
        OnChangeScore(score);
    }
    void OnChangeScore(int scre)
    {
        Debug.Log("Client Score Changed");
        score = scre;
        setScore();
    }

 void OnGUI()
    {
        if(isLocalPlayer)
        {
            Debug.Log("GUIcreated");
            uname = GUI.TextField(new Rect(25, Screen.height - 40, 100, 30), uname);
            if(GUI.Button(new Rect(130,Screen.height-40,80,30),"Change"))
            {
                CmdChangeName(uname);
                
            }
        }
    }

   

    [Command]
    public void CmdChangeName(string newName)
    {
        uname = newName;
    }
}
