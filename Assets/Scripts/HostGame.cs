﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.UI;

public class HostGame : MonoBehaviour {
    [SerializeField]
    private uint roomSize = 3;

    private string roomName;

    public InputField changeofValue;

    NetworkManager networkManager;


    void Start()
    {
        changeofValue = FindObjectOfType<InputField>();
        networkManager = NetworkManager.singleton;
        if(networkManager.matchMaker==null)
        {
            networkManager.StartMatchMaker();
        }
        changeofValue.onValueChange.AddListener(delegate { ValueChangeCheck(); });
    }

    // Invoked when the value of the text field changes.
    public void ValueChangeCheck()
    {
        Debug.Log("Value Changed"+changeofValue.text);
        SetName(changeofValue.text);
    }

    

    public void SetName(string name)
    {
        roomName = name;
    }

    public void CreateRoom()
    {
        if(roomName!="" && roomName!=null)
        {
            Debug.Log("Created room " + roomName + " with roomsize " + roomSize + " of players.");
            networkManager.matchMaker.CreateMatch(roomName, roomSize, true,"","","",0,0, networkManager.OnMatchCreate);
            
        }
        else
        {
            Debug.Log("input not received");
        }
    }

    void OnMatchCreate(bool success, string info, MatchInfo matchInfoData)
    {
        if (success && matchInfoData != null)
        {
            NetworkServer.Listen(matchInfoData, 777);
            //Start Host
            NetworkManager.singleton.StartHost(matchInfoData);
        }
        else
        {
            Debug.LogError("Create match failed : " + success + ", " + info);
        }
    }
}
