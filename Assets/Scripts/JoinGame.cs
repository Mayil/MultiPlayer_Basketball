﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.UI;

public class JoinGame : MonoBehaviour {
    List<GameObject> RoomList = new List<GameObject>();

    [SerializeField]
    Text status;
    [SerializeField]
    GameObject buttonPrefab;
    [SerializeField]
    Transform roomParent;

    NetworkManager networkManager;
    // Use this for initialization
    void Start () {
        networkManager = NetworkManager.singleton;
        if (networkManager.matchMaker == null)
        {
            networkManager.StartMatchMaker();
        }

        RefreshRoomList();
    }
	
    public void RefreshRoomList()
    {
        ClearRoomList();
        networkManager.matchMaker.ListMatches(0, 20, "",false,0,0,OnMatchList);
        status.text = "Loading...";
    }


    public void OnMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matchList)
    {
        //if (success)
        {
           // if (matchList.Count != 0)
            {
                //Debug.Log("A list of matches was returned");
                //join the last server (just in case there are two...)
               // NetworkManager.singleton.matchMaker.JoinMatch(matchList[0].networkId, "", "", "", 0, 0, OnjoinedMatch);
            }
            //else
            {
                //Debug.Log("No matches in requested room!");
            }
        }
        //else
        {
            //Debug.LogError("Couldn't connect to match maker");
        }
        status.text = "";
        if (matchList == null)
        {
            status.text = "Couldn't connect";
            return;
        }


        foreach (MatchInfoSnapshot match in matchList)
        {
            GameObject _roomListGO = Instantiate(buttonPrefab);
            _roomListGO.transform.SetParent(roomParent);


            RoomListButton _roomlist = _roomListGO.GetComponent<RoomListButton>();
            if(_roomlist!=null)
            {
                _roomlist.Setup(match,JoinRoom);
            }


            RoomList.Add(_roomListGO);
        }
    
        if (RoomList.Count == 0)
        {
            status.text = "No Rooms available";
        }
    }

    void JoinRoom(MatchInfoSnapshot match)
    {
        Debug.Log("Joined");       
        NetworkManager.singleton.matchMaker.JoinMatch(match.networkId, "", "", "", 0, 0, OnjoinedMatch);
    }

    void OnjoinedMatch(bool success, string info, MatchInfo matchInfoData)
    {
        if (success)
        {
            //Debug.Log("Able to join a match");
            NetworkManager.singleton.StartClient(matchInfoData);
            ClearRoomList();
            status.text = "JOINING...";
        }
        else
        {
            Debug.LogError("Join match failed");
        }
    }
    public void ClearRoomList()
    {
        for(int i=0;i<RoomList.Count;i++)
        {
            Destroy(RoomList[i]);
        }
        RoomList.Clear();
    }
}
